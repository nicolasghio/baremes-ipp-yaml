# Recensement des actualisations des barèmes IPP

Ci-dessous se trouve un tableau recensant pour chaque barème la liste des actualisations réalisées. Chaque fois que vous faites une actualisation d'un barème donné, vous devez renseigner à la ligne correspondant au barème :
- votre nom
- la date de l'actualisation
- la nature de l'actualisation (actualisation d'un dispositif en particulier seulement, actualisation de tout le barème, etc.).
- le cas échéant, la merge request (avec le lien hypertexte) associée à vos changements.

Même lorsque vous n'avez fait aucun changement dans les barèmes lors d'une actualisation du fait que la législation n'avait pas changé, vous devez quand même remplir ce tableau afin que la prochaine personne voulant utiliser ou actualiser le barème sache dans quelle mesure celui-ci est à jour.

Afin d'avoir tout l'historique des actualisations, il ne faut pas supprimer les lignes d'avant présentes pour le barème que vous avez actualisé. Pour cela, voici un exemple de code d'une multiligne en markdown :

```
| Barème            | Date        | Nom         | Nature de l'atualisation | Merge request
| -----------       | --------    | --------    | ------                   | --------
| Prélèvements sociaux  | 2021-01-01 <br>2022-01-01  | blabla <br>blabla | blabla <br>blabla | blabla <br>blabla
```
Donne ceci :
| Barème            | Date        | Nom         | Nature de l'atualisation | Merge request
| -----------       | --------    | --------    | ------                   | --------
| Prélèvements sociaux  | 2021-01-01 <br>2022-01-01  | blabla <br>blabla | blabla <br>blabla | blabla <br>blabla


# Tableau des actualisations des barèmes IPP

| Barème                          | Date        | Nom            | Nature de l'atualisation  | Merge request
| -----------                     | --------    | --------       | ------                    | --------
| Prélèvements sociaux            |             |                |                           | 
| Impôt sur le revenu             | 2020-08-31  | Gautier, Brice @bfabre | Vérification complète     | !202
| Taxation du capital             | 2021-01-14  | Brice @bfabre  | Vérification complète     |
| Taxation indirecte              |             |                |                           | 
| Fiscalité des entreprises       |             |                |                           | 
| Prestations sociales            | 2020-08-29  | Gautier, Brice @bfabre | Vérification complète | !203
| Retraites                       |             |                |                           | 
| Chômage                         |             |                |                           | 
| Marché du travail               |             |                |                           | 
| Tarifs réglementés de l'énergie |             |                |                           | 
